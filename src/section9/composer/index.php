<?php
require_once("vendor/autoload.php");

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('banco_de_dados');
$log->pushHandler(new StreamHandler('path/to/warning.log', Logger::WARNING));

$log->warning('File not found!!');
$log->error('Error when open files!');
?>
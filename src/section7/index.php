<?php
    $arrayIds = [];
    if(filter_input(INPUT_POST, "btnSubmit", FILTER_SANITIZE_STRING )){
        $checkboxChecked = filter_input(INPUT_POST, "ckUsuarios", FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);
        
        foreach($checkboxChecked as $ck){
            $arrayIds[] = $ck;
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Checkbox Practice</title>
</head>
<body>
    <div style="padding: 10px; background-color: #EEE;">
        <form method="POST">
            <label>
                <input type="checkbox" name="ckUsuarios[]" value='1' />
                pessoa 1
            </label>
            <label>
                <input type="checkbox" name="ckUsuarios[]" value=2 />
                pessoa 2
            </label>
            <label>
                <input type="checkbox" name="ckUsuarios[]" value=3 />
                pessoa 3
            </label>
            <label>
                <input type="checkbox" name="ckUsuarios[]" value=4 />
                pessoa 4
            </label>
            <label>
                <input type="checkbox" name="ckUsuarios[]" value=5 />
                pessoa 5
            </label>
            <br>
            <input type="submit" name="btnSubmit" value="Enviar" style="padding:5px;" />
        </form>
    </div>
    <br>
    <div style="padding: 10px; background-color: #EEE;">
        <?php
            echo "entrou no php inferior";
            for($i = 0; $i < count($arrayIds); $i++){
                echo "<p>ID selecionado: {$arrayIds[$i]}</p>";
            }
        ?>
    </div>
</body>
</html>
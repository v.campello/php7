<?php

require_once("services/UserService.php");

class UserController {

    private $userService;

    public function __construct() {
        $this->userService = new UserService();
    }

    public function Cadastrar(User $usuario) {
        if (strlen($usuario->getNome()) > 3 && strlen($usuario->getSenha()) >= 7 && strpos($usuario->getEmail(), "@") > 0) {
            return $this->userService->Cadastrar($usuario);
        } else {
            return -2; //Dados inválidos
        }
    }

    public function Autenticar(string $email, string $senha) {
        if (strpos($email, "@") > 0 && strpos($email, ".") > 0 && strlen($senha) >= 7) {
            return $this->userService->Autenticar($email, $senha);
        } else {
            return null;
        }
    }

    public function RetornarUsuario(string $email) {
        if (strpos($email, "@") > 0 && strpos($email, ".") > 0) {
            return $this->userService->RetornarUsuario($email);
        } else {
            return null;
        }
    }

}

?>
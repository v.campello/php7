<?php

// $nome = $_GET["nome"];
// echo $nome;

$arrayNome = ["Astolfo", "Berlinetta", "Lontreia", "Ivailson", "Omar"];
$nome = filter_input(INPUT_GET, "txtNome", FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_GET, "txtEmail", FILTER_SANITIZE_STRING);
$funcionarioCod = filter_input(INPUT_GET, "slFuncionario", FILTER_SANITIZE_NUMBER_INT);
$funcionario = "";

if ($funcionarioCod) {
    $funcionario = $arrayNome[ $funcionarioCod - 1 ];
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>Page Title</title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
		<script src="main.js"></script>
	</head>
	<body>
        <form method="GET">
            <ul>
                <li>Nome: <input type="text" name="txtNome" /></li>
                <li>E-mail: <input type="text" name="txtEmail" /></li>
                <li>
                    <select name="slFuncionario">
                        <?php
                        for ($i = 0; $i < count($arrayNome); $i++) { 
                           ?>
                           <option value="<?= ($i + 1); ?>"><?= $arrayNome[$i]; ?></option>
                           <?php
                        }
                        ?>
                    </select>
                </li>
                <li><input type="submit" name="btnSubmit" value="Cadastrar" /></li>
            </ul>
        </form>
        <br /><hr /><br />
        <p>Nome: <?= $nome; ?></p>
        <p>E-mail: <?= $email; ?></p>
        <p>Funcionário: <?= $funcionario; ?></p>      
	</body>
</html>
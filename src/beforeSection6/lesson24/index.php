<!-- <?php

$arrayNome = ["a", "b", "l", "i", "o"];
$nome = "";
$mail = "";
$funcionario ="";

if (isset($_POST["txtNome"])) {
    $nome = $_POST["txtNome"];
}

if (isset($_POST["txtEmail"])) {
    $email = $_POST["txtEmail"];
}

if (isset($_POST["slFuncionario"])) {
    $funcionario = $arrayNome[$_POST["slFuncionario"]];
}

?> -->
<?php

$arrayNome = ["a", "b", "l", "i", "o"];
$nome = filter_input(INPUT_POST, "txtNome", FILTER_SANITIZE_STRING);
$mail = filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_STRING);
$funcionarioCod = filter_input(INPUT_POST, "slFuncionario", FILTER_SANITIZE_NUMBER_INT);
$funcionario = "";

if ($funcionarioCod) {
    $funcionario = $arrayNome[ $funcionarioCod ];
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>Page Title</title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
		<script src="main.js"></script>
	</head>
	<body>
        <form method="post">
            <ul>
                <li>Nome: <input type="text" name="txtNome" /></li>
                <li>E-mail: <input type="text" name="txtEmail" /></li>
                <li>
                    <select name="slFuncionario">
                        <?php
                        for ($i = 0; $i < count($arrayNome); $i++) { 
                           ?>
                           <option value="<?= $i; ?>"><?= $arrayNome[$i]; ?></option>
                           <?php
                        }
                        ?>
                    </select>
                </li>
                <li><input type="submit" name="btnSubmit" value="Cadastrar" /></li>
            </ul>
        </form>
        <br /><hr /><br />
        <p>Nome: <?= $nome; ?></p>
        <p>E-mail: <?= $email; ?></p>
        <p>Funcionário: <?= $funcionario; ?></p>      
	</body>
</html>

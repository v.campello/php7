<?php

$cep = filter_input(INPUT_GET, "cep");
$str = file_get_contents('https://viacep.com.br/ws/' . $cep . '/' . 'json/');
$arrCidade = json_decode($str);
if($arrCidade!= null){
    echo '<p><b>CEP: </b> ' . $arrCidade -> cep . '</p>';
    echo '<p><b>LOGRADOURO: </b> ' . $arrCidade -> logradouro . '</p>';
    echo '<p><b>BAIRRO: </b> ' . $arrCidade -> bairro . '</p>';
    echo '<p><b>LOCALIDADE: </b> ' . $arrCidade -> localidade . '</p>';
}

?>
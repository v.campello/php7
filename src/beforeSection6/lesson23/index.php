<?php

$nome = "vinicius alves";
echo strtoupper($nome) . PHP_EOL;

$message = strip_tags("<h1>Welcome</h1>", "<h1>");
echo $message . PHP_EOL;

define("food" , "apple, lemon, pineapple");
$alt = str_replace("orange", "strawberry", food); /**useful with variables and constants */
echo $alt . "<br/>" . PHP_EOL;
echo food . "<br/>" . PHP_EOL;

define('age', 'age of 500');
echo age . "<br/>" . PHP_EOL;

$nome = "belo horizonte";
$ex = explode(" ", $nome);
echo $nome . "<br/>" . PHP_EOL;
echo $ex[0] . "<br/>" . PHP_EOL;
echo strlen($nome) . "<br/>" . PHP_EOL;

if (strlen($nome) >= 19) {
    echo $nome . "<br/>" . PHP_EOL;
} else {
    echo "menor que 19 chars" . "<br/>" . PHP_EOL;
}

?>

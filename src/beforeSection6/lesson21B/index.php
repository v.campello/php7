<?php
function ajuste($p1, $p2){
    return (($p1 * $p2) / 4);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>Page Title</title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
		<script src="main.js"></script>
	</head>
	<body>
        <!-- <?= ajuste(3, 10); ?> -->
		<ul>
            <?php
			for ($i = 0; $i < 10; $i++){
                ?>
                <li><?= ajuste($i, ($i + 6)) ?></li>
                <?php 
            } 
            ?>
        </ul>
	</body>
</html>
<?php

$arrExample = array(
    "name" => "vinicius",
    "level" => "6"
);
var_dump($arrExample);
$jsonStr = json_encode($arrExample);
echo "<br>" . $jsonStr . PHP_EOL;

$jsonReverseExample = '{"name":"vinicius","level":"6"}';
$arrUser = json_decode($jsonReverseExample);
var_dump($arrUser);
echo $arrUser;

?>